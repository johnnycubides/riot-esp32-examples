# Ejemplos con RIOT y ESP32

## Instalación de RIOT

Dependencias:

```bash
sudo apt update
sudo apt install git make doxygen wget unzip python3-serial curl python3 telnet picocom gtkterm
```
Dar permisos al grupo dialout a su usuario linux, ejemplo de comando:

```bash
sudo usermod -a -G dialout user_name
```

Donde **user_name** hace referencia al nombre de usuario con el que inició sesión.

Instalación de RIOT

```bash
cd ~
git clone https://github.com/RIOT-OS/RIOT.git
ln -sr RIOT riot-sdk
```

Instalación de herramientas de compilación y flasheao:

```bash
cd ~
cd RIOT
dist/tools/esptools/install.sh esp32
```

## Instalación de Ejemplos de RIOT

```bash
mkdir -p ~/projects/ && ~/projects/
git clone https://gitlab.com/johnnycubides/riot-esp32-examples.git
```

## Construcción del ejemplo blinky

```bash
cd ~/projects/riot-esp32-examples/blinky/
. environment.bash
make b f t
```

## Construyendo proyectos con GEANY

```bash
sudo apt install geany
```

```bash
[build-menu]
EX_00_LB=Flash
EX_00_CM=. ./environment.bash && make f gt
EX_00_WD=%d/
EX_01_LB=Serial
EX_01_CM=. ./environment.bash && make gt
EX_01_WD=%d/
NF_00_LB=_Make flash
NF_00_CM=. ./environment.bash && make f
NF_00_WD=%d/./
NF_01_LB=Make Custom _Target...
NF_01_CM=. ./environment.bash && make 
NF_01_WD=%d/../
FT_00_LB=_Compile
FT_00_CM=
FT_00_WD=
FT_01_LB=_Build
FT_01_CM=. ./environment.bash && make b
FT_01_WD=%d/
```

## Construyendo proyectos con Codelite

```bash
sudo apt install codelite codelite-plugins
```

## Referencias

Riot

* Riot docs: https://doc.riot-os.org/
* Get started: https://doc.riot-os.org/getting-started.html
* Toolchain esp32: https://doc.riot-os.org/group__cpu__esp32.html#esp32_toolchain
* Create app: https://doc.riot-os.org/creating-an-application.html
* Perifericos functions: https://doc.riot-os.org/group__drivers__periph.html
* Features esp32: https://doc.riot-os.org/group__cpu__esp32.html
* Properities esp32: https://doc.riot-os.org/group__cpu__esp32__esp32.html
* Agregando boards: https://doc.riot-os.org/porting-boards.html

examples:

* https://www.hackster.io/ichatz/control-external-led-using-riot-os-b626da
* https://riot-os.github.io/riot-course/slides/03-riot-basics/
* https://riot-os.github.io/riot-course/

Codelite

[Configuración del LSP](https://docs.codelite.org/plugins/lsp/#compile_flagstxt-compile_commandsjson)
