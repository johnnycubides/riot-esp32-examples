/*
 * Copyright (C) 2021 Otto-von-Guericke-Universität Magdeburg
 *
 * This file is subject to the terms and conditions of the GNU Lesser
 * General Public License v2.1. See the file LICENSE in the top level
 * directory for more details.
 */

/**
 * @ingroup     examples
 * @{
 *
 * @file
 * @brief       Blinky application
 *
 * @author      Marian Buschsieweke <marian.buschsieweke@ovgu.de>
 *
 * @}
 */

#include <stdio.h>

#include "board.h"
#include "periph_conf.h"
#include "ztimer.h"
#include "thread.h"

#define LED0_PIN GPIO22
#define LED1_PIN GPIO23
/* #define LED0_MASK       (BIT(LED0_PIN)) */
/* #define LED0_ON         (gpio_write(LED0_PIN,  LED0_ACTIVE)) */
/* #define LED0_OFF        (gpio_write(LED0_PIN, !LED0_ACTIVE)) */
#define LED0_TOGGLE     (gpio_toggle(LED0_PIN))
#define LED1_TOGGLE     (gpio_toggle(LED1_PIN))

#define DELAY_MS_LED0        50U
#define DELAY_MS_LED1        1000U


char task_led0_stack[THREAD_STACKSIZE_MAIN];

void *task_led0_thread(void *arg)
{
    (void) arg;
    puts("led0 thread\n");

    gpio_init(LED0_PIN, GPIO_OUT);

    while(1)
    {
        LED0_TOGGLE;
        ztimer_sleep(ZTIMER_MSEC, DELAY_MS_LED0);
        /* puts("LED0\n"); */
    }

    return NULL;
}

char task_led1_stack[THREAD_STACKSIZE_MAIN];

void *task_led1_thread(void *arg)
{
    (void) arg;
    puts("led1 thread\n");

    gpio_init(LED1_PIN, GPIO_OUT);

    while(1)
    {
        LED1_TOGGLE;
        ztimer_sleep(ZTIMER_MSEC, DELAY_MS_LED1);
        /* puts("LED1\n"); */
    }

    return NULL;
}

int main(void)
{

    (void) thread_create(
            task_led0_stack, sizeof(task_led0_stack),
            THREAD_PRIORITY_MAIN - 1,
            THREAD_CREATE_WOUT_YIELD | THREAD_CREATE_STACKTEST,
            task_led0_thread, NULL, "led0");

    (void) thread_create(
            task_led1_stack, sizeof(task_led1_stack),
            THREAD_PRIORITY_MAIN - 2,
            THREAD_CREATE_WOUT_YIELD | THREAD_CREATE_STACKTEST,
            task_led1_thread, NULL, "led1");

    puts("Threads created\n");
    return 0;
}
