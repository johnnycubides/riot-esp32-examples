# Blinky

Instrucciones de construcción
=============================

Acivar variables de entorno:

```bash
. environment.bash
```

Construir, flashear y monitorear:

```bash
make b f t
```
